from pynostr.pynostr.key import PrivateKey
from pynostr.pynostr.relay_manager import RelayManager
from pynostr.pynostr.event import EventKind, Event
from pynostr.pynostr.filters import FiltersList, Filters
import sys
import hashlib
import time
import uuid
from datetime import datetime, timedelta
import json

#coincurve, cryptography, requests, tlv8, tornado

discovery_file = r"discovery_keys.json"


# just extending pynostr Event, so it can handle arbytrary tag
def add_arbitrary_tag(self, tag, value):
    """Adds a arbitrary tag."""
    self.tags.append([tag, value])
    self.compute_id()

Event.add_arbitrary_tag = add_arbitrary_tag

def load_discovery_file():
    try:
        with open(discovery_file) as file:
            return json.load(file)
    except:
        return []

def append_discovery_file(discovery):
    data = load_discovery_file()
    with open(discovery_file, 'w') as file:
        data.append(discovery)
        json.dump(data, file)

def init_relay():
    relay_manager = RelayManager(timeout=60)
    #relay_manager.add_relay("wss://relay.current.fyi") #nostream
    relay_manager.add_relay("wss://relay.damus.io") #strfry
    #relay_manager.add_relay("wss://relay.snort.social") #nostr-rs-reay

    return relay_manager

def discover(discover_dict):
    hash_actual = make_salted_hash(discover_dict, datetime.now())
    hash_last = make_salted_hash(discover_dict, datetime.now() - timedelta(days = datetime.now().day))

    relay_manager = init_relay()

    print("hash actual:" + hash_actual)
    print("hash last:" + hash_last)

    filter = Filters(limit=100)
    filter.add_arbitrary_tag('d', [hash_actual])

    filters = FiltersList([filter])

    subscription_id = uuid.uuid1().hex
    relay_manager.add_subscription_on_all_relays(subscription_id, filters)

    relay_manager.run_sync()

    while relay_manager.message_pool.has_ok_notices():
        ok_msg = relay_manager.message_pool.get_ok_notice()
        print(ok_msg)
        print("1")

    print("discovery results:")

    while relay_manager.message_pool.has_events():
        event_msg = relay_manager.message_pool.get_event()
        print("pubkey: " + event_msg.event.to_dict()["pubkey"])
        #print(event_msg.event.to_dict())



    relay_manager.close_all_relay_connections()

def upload(discover_dict):
    hash = make_salted_hash(discover_dict, datetime.now())
    privkey = PrivateKey()
    discover_dict["privkey"] = privkey.nsec

    pubkey = privkey.public_key
    print ("puvkey: " + pubkey.hex())
    print("hash: " + hash)
    relay_manager = init_relay()

    filters = FiltersList([Filters(authors=[pubkey.hex()], limit=10)])
    subscription_id = uuid.uuid1().hex
    relay_manager.add_subscription_on_all_relays(subscription_id, filters)

    append_discovery_file(discover_dict)


    event = Event()

    event.add_arbitrary_tag('d', hash)
    event.sign(privkey.hex())

    relay_manager.publish_event(event)
    relay_manager.run_sync()
    time.sleep(5)

    while relay_manager.message_pool.has_ok_notices():
        ok_msg = relay_manager.message_pool.get_ok_notice()
        print(ok_msg)
    while relay_manager.message_pool.has_events():
        event_msg = relay_manager.message_pool.get_event()
        print(event_msg.event.to_dict())

    relay_manager.close_all_relay_connections()


def make_salted_hash(discover_dict, date):
    discover_dict["year"] = date.year
    discover_dict["month"] = date.month

    return hashlib.sha256("{}-{}:{}{}".format(discover_dict["month"], discover_dict["year"], discover_dict["type"], discover_dict["id"]).encode(encoding="utf-8")).hexdigest()


def make_discover_dict(type, id):
    return {"type": type, "id": id.strip().lower()}


if len(sys.argv) == 1 or sys.argv[1] == "--help":
    print("-um x@y.z upload email to server\n-dm x@y.z discover email on relay")
    exit(0)
if "-um" == sys.argv[1] or "--upload_mail" == sys.argv[1]:
    upload(make_discover_dict("mail", sys.argv[2]))
if "-dm" == sys.argv[1] or "--discover_mail" == sys.argv[1]:
    discover(make_discover_dict("mail", sys.argv[2]))
